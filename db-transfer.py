import argparse
import couchdb

BULK_INSERT_SIZE = 1000
config = {}


def bulk_insert(db, data):
    print("Bulk inserting document of size '{}' to db".format(len(data)))
    try:
        for doc in db.update(data):
            #print(repr(doc))
            if doc[0] is False:
                print(repr(doc))
    except:
        print("error")


# Creates db in couchDB with name dbname if not exist, then returns the database.
def get_db(dbname, server):
    if dbname in server:
        db = server[dbname]
    else:
        db = server.create(dbname)
    return db


def init_couch_db(env):
    host = env["chost"]
    port = env["cport"]
    user = env["cuname"]
    password = env["cpass"]
    server = couchdb.Server("http://{}:{}@{}:{}/".format(user, password, host, port))
    print("Couch db connected at {}".format(server))
    return server


# Parse command line environment variables
def parse_env():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dbfrom", help="database to get data from")
    parser.add_argument("--dbto", help="datbase to move to")

    parser.add_argument("--chost", help="couch db host")
    parser.add_argument("--cport", help="couch db port")
    parser.add_argument("--cuname", help="couch db username")
    parser.add_argument("--cpass", help="couch db password")

    parser.add_argument("--id", nargs='?', default=0, type=int,
                        help="id of this process (when running multiple simultaneous db-transfers)")
    parser.add_argument("--n", nargs='?', default=1, type=int, help="number of processes")

    args = parser.parse_args()

    env = {"dbfrom": args.dbfrom, "dbto": args.dbto,
           "chost": args.chost, "cport": args.cport, "cuname": args.cuname, "cpass": args.cpass,
           "id": args.id, "n": args.n}
    return env


def transfer_data(db_from, db_to, process_id, process_num):
    bulk_docs = []
    doc_count = 0
    limit = 10000

    while True:
        try:
        view = db_from.view('_all_docs', limit=limit, skip=doc_count)
        for item in view:
            print(doc_count)
            doc_id = item.key
            doc = db_from[doc_id]
            del (doc["_rev"])

            if doc_count % process_num == process_id: # and doc["custom"]["coordinates"] is not None:
                bulk_docs.append(doc)

            if len(bulk_docs) >= BULK_INSERT_SIZE:
                bulk_insert(db_to, bulk_docs)
                bulk_docs = []

            doc_count += 1

        if len(view) < limit:
            break

    if len(bulk_docs) > 0:
        bulk_insert(db_to, bulk_docs)


def main():
    env = parse_env()
    couchdb_server = init_couch_db(env)

    db_from = get_db(env["dbfrom"], couchdb_server)
    db_to = get_db(env["dbto"], couchdb_server)
    transfer_data(db_from, db_to, env["id"], env["n"])


if __name__ == "__main__":
    main()

# nohup python3 database-transfer.py --dbfrom tweets_2 --dbto tweets_2_geo --chost 115.146.84.215 --cport 5984 --cuname inst1 --cpass riochin --id 0 --n 8 > results/db-transfer-1.out
# nohup python3 database-transfer.py --dbfrom tweets_2 --dbto tweets_2_geo --chost 115.146.84.215 --cport 5984 --cuname inst1 --cpass riochin --id 1 --n 8 > results/db-transfer-2.out
# nohup python3 database-transfer.py --dbfrom tweets_2 --dbto tweets_2_geo --chost 115.146.84.215 --cport 5984 --cuname inst1 --cpass riochin --id 2 --n 8 > results/db-transfer-3.out
# nohup python3 database-transfer.py --dbfrom tweets_2 --dbto tweets_2_geo --chost 115.146.84.215 --cport 5984 --cuname inst1 --cpass riochin --id 3 --n 8 > results/db-transfer-4.out
# nohup python3 database-transfer.py --dbfrom tweets_2 --dbto tweets_2_geo --chost 115.146.84.215 --cport 5984 --cuname inst1 --cpass riochin --id 4 --n 8 > results/db-transfer-5.out
# nohup python3 database-transfer.py --dbfrom tweets_2 --dbto tweets_2_geo --chost 115.146.84.215 --cport 5984 --cuname inst1 --cpass riochin --id 5 --n 8 > results/db-transfer-6.out
# nohup python3 database-transfer.py --dbfrom tweets_2 --dbto tweets_2_geo --chost 115.146.84.215 --cport 5984 --cuname inst1 --cpass riochin --id 6 --n 8 > results/db-transfer-7.out
# nohup python3 database-transfer.py --dbfrom tweets_2 --dbto tweets_2_geo --chost 115.146.84.215 --cport 5984 --cuname inst1 --cpass riochin --id 7 --n 8 > results/db-transfer-8.out


# DOC_LIMIT = 100000
# doc_count = 0
# while True:
#
#     for doc in db.view(limit=DOC_LIMIT, skip=doc_count):
#         doc_count += 1
#         doc_list.append(doc)


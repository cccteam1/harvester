import config
import couchdb
import time
import reverse_geocoder as rg
import tweepy
from sentiment_analysis import sentiment_analysis


# --- FILTER functions start ---
# Returns true if tweet was created after specified date
def after_date(tweet, date):
    return tweet.created_at > date


# Returns true if any hashtags of a tweet belongs to the specified hashtags a tweet should contain
def has_hashtags(tweet, hashtags):
    tweet_hashtags = list(map(lambda x: x["text"].lower(), tweet.entities.get("hashtags")))
    for tweet_hashtag in tweet_hashtags:
        if tweet_hashtag in hashtags:
            return True
    return False


# Returns true if user is account is irrelevant, if there is too little tweets that contains the specified hashtags
def is_irrelevant_user(hashtags_count):
    return hashtags_count < config.MIN_HASHTAGS_COUNT


# Returns true if user has already been harvested/processed before
def was_processed(user):
    return user in get_harvested_users()


# Returns true if tweet has already been processed before
def was_harvested(tweet):
    try:
        return str(tweet.id) in get_db(config.ALL_TWEETS_DB_NAME)
    except AttributeError:
        return False
# --- FILTER functions end ---


def bulk_insert_tweets(tweet_docs):
    db = get_db(config.ALL_TWEETS_DB_NAME)
    for (success, doc_id, revision_or_exception) in db.update(tweet_docs):
        if not success:
            print("Conflict ID: {}".format(doc_id))


def convert_to_couchdb_doc(tweet):
    doc_id = str(tweet.pop("id", None))
    tweet["_id"] = doc_id
    return tweet


# Creates db in couchDB with name dbname if not exist, then returns the database.
def get_db(dbname):
    if dbname in config.couch_server:
        db = config.couch_server[dbname]
    else:
        db = config.couch_server.create(dbname)
    return db


def get_harvested_users():
    dbname = config.ADDITIONAL_INFO_DB_NAME
    db = get_db(dbname)
    processed_users = []
    if config.USERNAMES_DOC_ID in db:
        processed_users = db[config.USERNAMES_DOC_ID][config.USERNAMES_KEY]
    return processed_users


def init_config_properties(env):
    config.couch_server = config.init_couch_db(env)
    config.twitter_api = config.init_tweepy_api(env)
    config.emoji_ranker = sentiment_analysis.get_emoji_rank()
    config.sentiment_analyzer = sentiment_analysis.get_sentiment_analyzer()

    config.ALL_TWEETS_DB_NAME = env["alldb"]
    config.GEO_TWEETS_DB_NAME = env["geodb"]
    config.USERNAMES_DOC_ID = env["usrdoc"]

    config.DATE_FILTER_ON = env["fd"]
    config.HASHTAG_FILTER_ON = env["fh"]
    config.MIN_TWEET_WITH_HASHTAGS_ON = env["fmh"]
    config.SKIP_PROCESSED_USERS = env["fsu"]
    config.SKIP_USER_WHEN_FOUND_SAME_TWEET = env["fsut"]
    config.TWEET_COUNT_FILTER_ON = env["ftc"]


# Store tweet according to it's location data
def insert_tweet_into_db(doc):
    # if doc["location_data"] is not None:
    #     dbname = config.GEO_TWEETS_DB_NAME
    # else:
    dbname = config.ALL_TWEETS_DB_NAME
    db = get_db(dbname)
    doc_id = str(doc.pop("id", None))
    db[doc_id] = doc
    # print("Inserted tweet {} into {}".format(doc, dbname))


# Stores and updates list of users in the same document
def insert_user_into_db(username):
    dbname = config.ADDITIONAL_INFO_DB_NAME
    db = get_db(dbname)
    processed_users = get_harvested_users()

    if username in processed_users:
        return
    processed_users.append(username)

    if config.USERNAMES_DOC_ID in db:
        document_data = db.get(config.USERNAMES_DOC_ID)
        document_data[config.USERNAMES_KEY] = processed_users
    else:
        document_data = {config.USERNAMES_KEY: processed_users}

    db[config.USERNAMES_DOC_ID] = document_data
    print("Inserted user {} into {}".format(username, dbname))


# Get only relevant information from tweet
def parse_tweet(tweet, following_user_name=None):
    coordinates = None
    user_location = None
    try:
        user_location = tweet.user.location
        coordinates = tweet.coordinates.get("coordinates")
    except AttributeError:
        pass

    tweet_dic = {"coordinates": coordinates,
                 "created_at": time.mktime(tweet.created_at.utctimetuple()),
                 "following": following_user_name,
                 "hashtags": list(map(lambda x: x["text"], tweet.entities.get("hashtags"))),
                 "id": tweet.id,
                 "text": tweet.text,
                 "user": tweet.user.screen_name,
                 "user_location": user_location}

    tweet_json_with_dic = tweet._json.copy()
    tweet_json_with_dic.update({"custom": tweet_dic})
    return tweet_json_with_dic


# Pre-process tweet (get suburb, perform sentiment analysis, etc.)
def process_tweet(tweet):
    tweet["custom"]["sentiment_score"] = get_sentiment(tweet["text"])
    # tweet["suburb"] = get_suburb(map_data, tweet["coordinates"])
    tweet["custom"]["location_data"] = get_location_data(tweet["custom"]["coordinates"])

    # Remove redundant information from tweet
    del(tweet["coordinates"])

    return tweet


# Get sentiment score
def get_sentiment(text):
    return sentiment_analysis.sentiment_analysis(text, config.sentiment_analyzer, config.emoji_ranker)


# Get location data when passed coordinate in format of [lon, lat]
def get_location_data(coordinate):
    try:
        if coordinate is None or None in coordinate:
            return None

        geocode_data = rg.search((coordinate[1], coordinate[0]))[0]
        location_data = {"name": geocode_data["name"], "admin1": geocode_data["admin1"],
                         "admin2": geocode_data["admin2"]}
        return location_data
    except (KeyError, IndexError, TypeError) as e:
        print("Coordinate: {} gives error {}".format(coordinate, e))
        return None


# Stores tweets from user's timeline (and user's followers if followers_depth > 0)
def get_user_tweets(user_screen_name, following_user_name, followers_depth):
    print("Reading user: {}".format(user_screen_name))
    tweet = None
    tweets_to_store = []

    hashtags_count = 0
    tweet_count = 0

    tweet_was_harvested = False
    start_time = time.time()

    cursor = tweepy.Cursor(config.twitter_api.user_timeline, screen_name=user_screen_name).items()
    while True:
        try:
            tweet = cursor.next()
            tweet_count += 1
            print("Tweet #{}: {}".format(tweet_count, tweet))

            if has_hashtags(tweet, config.hashtags_filter):
                hashtags_count += 1

            if config.TWEET_COUNT_FILTER_ON and tweet_count > config.MAX_TWEET_PER_USER:
                raise StopIteration
            if tweet_count % 25 == 0 and was_harvested(tweet):
                tweet_was_harvested = True
                if config.SKIP_USER_WHEN_FOUND_SAME_TWEET:
                    print("User tweet has been processed, skipping {}".format(user_screen_name))
                    raise StopIteration
                else:
                    continue

            parsed_tweet = parse_tweet(tweet, following_user_name)
            processed_tweet = process_tweet(parsed_tweet)

            tweets_to_store.append(convert_to_couchdb_doc(processed_tweet))
            print("Processed tweet #{} from {}".format(tweet_count, user_screen_name))

        except StopIteration:
            bulk_insert_tweets(tweets_to_store)
            print("Completed storing {} tweets in {} seconds".format(tweet_count, time.time() - start_time))

            # Get tweets from followers of current users
            if followers_depth > 0 and not tweet_was_harvested:
                get_followers_tweets(user_screen_name, followers_depth - 1)
            break
        except (AttributeError, KeyError, TypeError,
                couchdb.http.ResourceConflict, couchdb.ServerError) as error:
            print("Error encountered: {} for tweet: {}".format(error, tweet))
        except tweepy.TweepError as error:
            print("Error encountered: {} for tweet: {}".format(error, tweet))
            break
        except (TimeoutError, ConnectionError, ConnectionResetError, ConnectionAbortedError,
                ConnectionRefusedError) as error:
            print("Error encountered: {} when going through user tweets, started sleep for 1 min".format(error))
            time.sleep(60)
            print("Sleep finished")
            continue


# Stores tweets from the timeline of all the user's followers using BFS (followers_depth indicates depth of BFS)
def get_followers_tweets(user_screen_name, followers_depth=1, starting=False):
    print("Getting tweets from followers of {}".format(user_screen_name))
    followers_count = 0

    # Get tweets from followers of current user
    cursor = tweepy.Cursor(config.twitter_api.followers, screen_name=user_screen_name).items()
    while True:
        try:
            follower = cursor.next()
            print(starting)
            if not starting and followers_count == config.MAX_FOLLOWERS:
                break

            get_user_tweets(follower.screen_name, user_screen_name, followers_depth)
            followers_count += 1
        except StopIteration:
            break
        except (AttributeError, KeyError, TypeError,
                couchdb.http.ResourceConflict, couchdb.ServerError) as error:
            print("Error encountered: {} when going through followers".format(error))
        except tweepy.TweepError as error:
            print("Error encountered: {} when going through followers".format(error))
            break
        except (TimeoutError, ConnectionError, ConnectionResetError,
                ConnectionAbortedError, ConnectionRefusedError) as error:
            print("Error encountered: {} when going through followers, trying again in 1 min".format(error))
            time.sleep(60)
            print("Sleep finished")
            continue


# Class for using Twitter stream api
class MyStreamListener(tweepy.StreamListener):
    def on_status(self, status):
        try:
            if not was_harvested(status):
                parsed_tweet = parse_tweet(status)
                processed_tweet = process_tweet(parsed_tweet)
                insert_tweet_into_db(processed_tweet)
        except (AttributeError, KeyError, TypeError, tweepy.TweepError, ConnectionError, ConnectionResetError,
                ConnectionAbortedError, ConnectionRefusedError, couchdb.http.ResourceConflict) as error:
            print("Error encountered: {}".format(error))
            pass

    def on_error(self, status_code):
        print("Stream error: {}".format(status_code))
        # # returning False in on_data disconnects the stream
        # return False

    def on_exception(self, exception):
        print("Stream exception: {}".format(exception))
        return


# Main function to run first
def main():
    try:
        env = config.parse_env()
        init_config_properties(env)

        print(env)

        # Decide whether to use stream api or search by followers
        if env["stream"]:
            my_stream = tweepy.Stream(auth=config.twitter_api.auth, listener=MyStreamListener())
            my_stream.filter(track=[env["squery"]])
        else:
            get_followers_tweets(env["twitteraccount"], starting=True)
    except ValueError as error:
        print(error)

    print("Finished running harvester")


if __name__ == "__main__":
    main()


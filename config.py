import argparse
import couchdb
import json
import tweepy

from datetime import datetime

ALL_TWEETS_DB_NAME = "tweets"
GEO_TWEETS_DB_NAME = "geo_tweets"
ADDITIONAL_INFO_DB_NAME = "additional_info"

USERNAMES_DOC_ID = "processed_users"
USERNAMES_KEY = "usernames"

# Tweet filters
date_limit = datetime.strptime("Jan 01 2017 00:00:00", "%b %d %Y %H:%M:%S")
hashtags_filter = ["auspol"]
DEFAULT_STREAM_QUERY = "#auspol"
MIN_HASHTAGS_COUNT = 1
MIN_TWEET_WITH_HASHTAGS = 50
MAX_TWEET_PER_USER = 200
MAX_FOLLOWERS = 100

# Tweet filters settings
DATE_FILTER_ON = False
HASHTAG_FILTER_ON = False
MIN_TWEET_WITH_HASHTAGS_ON = False
SKIP_PROCESSED_USERS = False
SKIP_USER_WHEN_FOUND_SAME_TWEET = True
TWEET_COUNT_FILTER_ON = True

# Database
couch_server = None

# Sentiment analysis objects
emoji_ranker = None
sentiment_analyzer = None

# Twitter api object
twitter_api = None

# Geo data of Victoria
victoria_map = []

def init_couch_db(env):
    host = env["chost"]
    port = env["cport"]
    user = env["cuname"]
    password = env["cpass"]
    server = couchdb.Server("http://{}:{}@{}:{}/".format(user, password, host, port))
    print("Couch db connected at {}".format(server))
    return server


def init_map():
    with open("victoria-map.json") as victoria_map_file:
        map_json = json.load(victoria_map_file)
        return map_json["features"]


def init_tweepy_api(env):
    with open(env["tk"]) as twitter_key_file:
        keys = twitter_key_file.readline().split(" ")
        auth = tweepy.OAuthHandler(keys[0], keys[1])
        auth.set_access_token(keys[2], keys[3])
        tweepy_api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
        return tweepy_api


def str_to_bool(string):
    return string.lower() == "true"


# Parse command line environment variables
def parse_env():
    parser = argparse.ArgumentParser()

    # Twitter api arguments
    parser.add_argument("--tk", help="twitter key file - txt file containing: "
                                     "consumer_key consumer_secret access_token access_token_secret")

    # Database arguments
    parser.add_argument("--chost", help="couch db host")
    parser.add_argument("--cport", help="couch db port")
    parser.add_argument("--cuname", help="couch db username")
    parser.add_argument("--cpass", help="couch db password")

    parser.add_argument("--alldb", nargs='?', default=ALL_TWEETS_DB_NAME, help="db with all tweets")
    parser.add_argument("--geodb", nargs='?', default=GEO_TWEETS_DB_NAME, help="db with geo tweets")
    parser.add_argument("--usrdoc", nargs='?', default=USERNAMES_DOC_ID,
                        help="doc id for storing processed users")

    # Filter arguments
    parser.add_argument("--fd", nargs='?', type=str_to_bool, default=DATE_FILTER_ON, help="filter date (default: False)")
    parser.add_argument("--fh", nargs='?', type=str_to_bool, default=HASHTAG_FILTER_ON,
                        help="filter hashtags (default: False)")
    parser.add_argument("--fmh", nargs='?', type=str_to_bool, default=MIN_TWEET_WITH_HASHTAGS_ON,
                        help="filter minimum hashtags (default: False)")
    parser.add_argument("--fsu", nargs='?', type=str_to_bool, default=SKIP_PROCESSED_USERS,
                        help="filter stop user harvest when found processed user (default: False)")
    parser.add_argument("--fsut", nargs='?', type=str_to_bool, default=SKIP_USER_WHEN_FOUND_SAME_TWEET,
                        help="filter stop user harvest when found processed tweet (default: True")
    parser.add_argument("--ftc", nargs='?', type=str_to_bool, default=TWEET_COUNT_FILTER_ON,
                        help="filter tweet count (default: True)")

    parser.add_argument("--twitteraccount", nargs='?', help="root twitter account")
    parser.add_argument("--stream", nargs='?', type=str_to_bool, default=False, help="use stream api")
    parser.add_argument("--squery", nargs='?', default=DEFAULT_STREAM_QUERY, help="stream query (default: #auspol)")
    args = parser.parse_args()

    env = {"tk": args.tk,
           "chost": args.chost, "cport": args.cport, "cuname": args.cuname, "cpass": args.cpass,
           "alldb": args.alldb, "geodb": args.geodb, "usrdoc": args.usrdoc,
           "fd": args.fd, "fh": args.fh, "fmh": args.fmh, "fsu": args.fsu, "fsut": args.fsut, "ftc": args.ftc,
           "twitteraccount": args.twitteraccount,
           "stream": args.stream, "squery": args.squery}
    return env

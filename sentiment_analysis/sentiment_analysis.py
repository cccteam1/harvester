import re
import csv
from textblob import TextBlob
from textblob import Blobber
from textblob.sentiments import NaiveBayesAnalyzer
from emoji import UNICODE_EMOJI
import time

"""
twitter = "The Buddha, the U.S.A., resides quite as comfortably in the circuits of a digital
computer or the gears of a cycle transmission as he does at the top of a mountain
or in the petals of a flower. To think otherwise is to demean the Buddha...which is
to demean oneself ."

twitter1 = ""I would be very careful. because smiley face emotions as associated with negative #sentiment as positive""


twitter2 = ""This is awlful, such a terrible people""

twitter3 = ""I don't like it, since it is not a good idea @happyday""

twitter4 = ""🤔 🙈 me así, bla es se 😌 ds 💕👭👙""

"""


def classify_polarity(polarity):
    if polarity > 0.2:
        return 'positive'
    elif polarity < -0.2:
        return 'negative'
    else:
        return 'neutral'


# since the sentiment of emoji is independent, we can analyse it separately without including it into machine learning
def emoji_analysis(twitter, emoji_rank):
    emo_polarity = 0
    total_count = 0
    for emoji in twitter:
        if emoji in emoji_rank:
            total_count += 1
            emo_polarity += emoji_rank[emoji]
    if total_count == 0:
        return None
    else:
        # average of emoji's sentiment
        emo_polarity = emo_polarity / total_count
        return emo_polarity


def get_emoji_rank():
    # emoji sentiment rank from http://kt.ijs.si/data/Emoji_sentiment_ranking/
    with open('sentiment_analysis/Emoji_Sentiment_Data_v1.0.csv', newline='', encoding='utf-8') as csvfile:
        emoji_list = csv.reader(csvfile, delimiter=' ', quotechar='|')
        next(emoji_list, None)
        emoji_rank = {}
        for emoji in emoji_list:
            emoji = emoji[0].split(',')
            # only use emoji which occurs more than 50 times, less occurrences leads to unreliable result
            if int(emoji[2]) >= 50:
                # [0] is Emoji, [2] is Total Occurrence, [4] is Negative, [5] is Neutral, [6] is Positive
                # here multiplied by (1 - Neutral / Occurrence) to take neutral tweets into consideration
                emoji_rank[emoji[0]] = ((int(emoji[6]) - int(emoji[4])) / int(emoji[2])) * (1 - int(emoji[5]) / int(emoji[2]))
    return emoji_rank


def get_sentiment_analyzer():
    return Blobber(analyzer=NaiveBayesAnalyzer())


def sentiment_analysis(tweet, sentiment_analyzer, emoji_rank):
    emoji_factor = 0.5
    emoji_polarity = emoji_analysis(tweet, emoji_rank)
    tweet = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())
    sentiment = sentiment_analyzer(tweet).sentiment
    text_polarity = sentiment.p_pos - sentiment.p_neg

    if emoji_polarity is not None:
        polarity = emoji_polarity * emoji_factor + text_polarity * (1 - emoji_factor)
        return classify_polarity(polarity)
    else:
        return classify_polarity(text_polarity)

# print("start")
# start_time = time.time()
# emoji_ranker = get_emoji_rank()
# sentiment_analyzer = Blobber(analyzer=NaiveBayesAnalyzer())
#
# tweet = "Fuck you"
# tweet2 = "I don't like it, since it is not a good idea @happyday"
# tweet3 = "🤔 🙈 me así, bla es se 😌 ds 💕👭👙 🤔 🙈 me así, bla es se 😌 ds 💕👭👙 🤔 🙈 me así, bla es se 😌 ds 💕👭👙"
#
# for i in range(1, 500):
#     print(sentiment_analysis(tweet, sentiment_analyzer, emoji_ranker))
# # print(sentiment_analysis(tweet, sentiment_analyzer, emoji_ranker))
# # print(sentiment_analysis(tweet2, sentiment_analyzer, emoji_ranker))
# # print(sentiment_analysis(tweet3, sentiment_analyzer, emoji_ranker))
#
# print("Time taken: {}".format(time.time() - start_time))

#sentiment_analysis(twitter1, get_emoji_rank())